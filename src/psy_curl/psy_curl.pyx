
# cython: infer_types=True
# cython: embedsignature=True
# cython: boundscheck=False

from posix.unistd cimport getpid, usleep, close, sysconf

from libc.stdlib cimport malloc, free, calloc
from libc.stdio cimport snprintf, printf
from libc.string cimport strerror, memset, memcpy
from libc.errno cimport ENOBUFS, errno, EAGAIN, EBADF, EINVAL
from libc.stdint cimport *


from cpython.ref cimport Py_XINCREF, Py_XDECREF
from cpython.int cimport PyInt_Check
from cpython.dict cimport PyDict_Check
from cpython.list cimport PyList_Check
from cpython cimport PyErr_SetFromErrno
from cpython.string cimport PyString_GET_SIZE, PyString_Check, \
        PyString_AS_STRING, PyString_FromStringAndSize, PyString_FromString
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free


cdef extern from "unistd.h" nogil:
    enum: _SC_PAGE_SIZE


cdef extern from "Python.h":
    ctypedef struct PyObject


cdef extern from "<curl/curl.h>":
    ctypedef enum CURLoption:
        CURLOPT_VERBOSE

        CURLOPT_READDATA

        CURLOPT_URL

        CURLOPT_PROXY
        CURLOPT_PROXYPORT
        CURLOPT_PROXYTYPE
        CURLOPT_HTTPPROXYTUNNEL

        CURLOPT_INTERFACE
        CURLOPT_LOCALPORT
        CURLOPT_DNS_CACHE_TIMEOUT
        CURLOPT_DNS_USE_GLOBAL_CACHE

        CURLOPT_BUFFERSIZE
        CURLOPT_PORT
        CURLOPT_TCP_NODELAY
        CURLOPT_TCP_KEEPALIVE
        CURLOPT_TCP_KEEPIDLE
        CURLOPT_TCP_KEEPINTVL

        CURLOPT_USERPWD
        CURLOPT_PROXYUSERPWD
        CURLOPT_USERNAME
        CURLOPT_PASSWORD

        CURLOPT_PROXYUSERNAME
        CURLOPT_PROXYPASSWORD

        CURLOPT_HTTPAUTH
        CURLOPT_PROXYAUTH

        CURLOPT_AUTOREFERER
        CURLOPT_ACCEPT_ENCODING
        CURLOPT_TRANSFER_ENCODING
        CURLOPT_FOLLOWLOCATION

        CURLOPT_UNRESTRICTED_AUTH

        CURLOPT_MAXREDIRS
        CURLOPT_POSTREDIR

        CURLOPT_HTTPGET
        CURLOPT_PUT
        CURLOPT_POST

        CURLOPT_REFERER
        CURLOPT_USERAGENT

        CURLOPT_HTTPHEADER

        CURLOPT_HTTP_VERSION

        CURLOPT_IGNORE_CONTENT_LENGTH
        CURLOPT_HTTP_TRANSFER_DECODING

        CURLOPT_TIMEOUT
        CURLOPT_TIMEOUT_MS

        CURLOPT_CONNECTTIMEOUT
        CURLOPT_CONNECTTIMEOUT_MS

    ctypedef enum CURLMcode:
        CURLM_OK

    ctypedef enum CURLcode:
        CURLE_OK

    struct curl_slist

    char *curl_easy_strerror(CURLcode errornum)
    char *curl_multi_strerror(CURLMcode  errornum )
    curl_slist *curl_slist_append(curl_slist *, char *)
    void curl_slist_free_all(curl_slist * list)


cdef extern from "cpsy_curl.h":

    enum param_type:
        LongParam
        CharPtrParam
        SlistParam

    struct mem_chunk_t:
        size_t size
        size_t used
        mem_chunk_t * next
        char * data

    struct tcurl_option_t:
        CURLoption option
        param_type p_type

        char * c_param
        long l_param
        curl_slist * slist_param

        void * pyobjref
        tcurl_option_t * next

    struct tcurl_task_t:
        int success
        void * id

        tcurl_option_t * options

        char * read_data
        uint64_t read_left

        CURLcode curl_ret
        int sys_errno

        # response
        char * headers
        uint16_t * headers_offset
        uint16_t headers_cnt

        int parse_headers

        unsigned int should_inflate


        size_t rsp_size
        mem_chunk_t * rsp_data_head
        mem_chunk_t * rsp_data_cur

        long response_code
        char * effective_url
        double total_time
        double namelookup_time
        double connect_time
        double speed_download
        long header_size
        char * primary_ip
        long primary_port

        tcurl_task_t * next

    struct thread_work_info_t:

        int thread_running

        int in_signal_fd[2]
        int out_signal_fd[2]

        int curls_num
        long maxconns

        CURLMcode curl_m_exit_code
        CURLcode curl_exit_code
        int sys_errno

    int psycurl_get_complete(thread_work_info_t * self, tcurl_task_t ** result)
    int psycurl_put_task(thread_work_info_t * self, tcurl_task_t * new_task)

    thread_work_info_t * psycurl_start_thread(int curls_num,
            long maxconns,
            int in_queue_size,
            int out_queue_size
            )
    void psycurl_stop_thread(thread_work_info_t * self)
    void psycurl_free_thread(thread_work_info_t * self)

    void mem_chunk_free(mem_chunk_t * chunk)




VERBOSE = CURLOPT_VERBOSE
READDATA = CURLOPT_READDATA
URL = CURLOPT_URL
PROXY = CURLOPT_PROXY
PROXYPORT = CURLOPT_PROXYPORT
PROXYTYPE = CURLOPT_PROXYTYPE
HTTPPROXYTUNNEL = CURLOPT_HTTPPROXYTUNNEL
INTERFACE = CURLOPT_INTERFACE
LOCALPORT = CURLOPT_LOCALPORT
DNS_CACHE_TIMEOUT = CURLOPT_DNS_CACHE_TIMEOUT
DNS_USE_GLOBAL_CACHE = CURLOPT_DNS_USE_GLOBAL_CACHE
BUFFERSIZE = CURLOPT_BUFFERSIZE
PORT = CURLOPT_PORT
TCP_NODELAY = CURLOPT_TCP_NODELAY
TCP_KEEPALIVE = CURLOPT_TCP_KEEPALIVE
TCP_KEEPIDLE = CURLOPT_TCP_KEEPIDLE
TCP_KEEPINTVL = CURLOPT_TCP_KEEPINTVL
USERPWD = CURLOPT_USERPWD
PROXYUSERPWD = CURLOPT_PROXYUSERPWD
USERNAME = CURLOPT_USERNAME
PASSWORD = CURLOPT_PASSWORD
PROXYUSERNAME = CURLOPT_PROXYUSERNAME
PROXYPASSWORD = CURLOPT_PROXYPASSWORD
HTTPAUTH = CURLOPT_HTTPAUTH
PROXYAUTH = CURLOPT_PROXYAUTH
AUTOREFERER = CURLOPT_AUTOREFERER
ACCEPT_ENCODING = CURLOPT_ACCEPT_ENCODING
TRANSFER_ENCODING = CURLOPT_TRANSFER_ENCODING
FOLLOWLOCATION = CURLOPT_FOLLOWLOCATION
UNRESTRICTED_AUTH = CURLOPT_UNRESTRICTED_AUTH
MAXREDIRS = CURLOPT_MAXREDIRS
POSTREDIR = CURLOPT_POSTREDIR
HTTPGET = CURLOPT_HTTPGET
PUT = CURLOPT_PUT
POST = CURLOPT_POST
REFERER = CURLOPT_REFERER
USERAGENT = CURLOPT_USERAGENT
HTTPHEADER = CURLOPT_HTTPHEADER
HTTP_VERSION = CURLOPT_HTTP_VERSION
IGNORE_CONTENT_LENGTH = CURLOPT_IGNORE_CONTENT_LENGTH
HTTP_TRANSFER_DECODING = CURLOPT_HTTP_TRANSFER_DECODING
TIMEOUT = CURLOPT_TIMEOUT
TIMEOUT_MS = CURLOPT_TIMEOUT_MS
CONNECTTIMEOUT = CURLOPT_CONNECTTIMEOUT
CONNECTTIMEOUT_MS = CURLOPT_CONNECTTIMEOUT_MS



class psy_exc_exception(Exception):
    pass

class psy_exc_queue_full(psy_exc_exception):
    pass


def easy_strerror(CURLcode err):
    return curl_easy_strerror(err)


def multi_strerror(CURLMcode err):
    return curl_multi_strerror(err)


cdef class HttpResponse:

    """
        HttpResponse objects created in
        CurlThread.get_complete() method

        Attribute success set to True
        when request successfully completed.
        Otherwise sys_errno or curl_ret is set.
    """

    cdef readonly:
        int success
        object id

        CURLcode curl_ret
        int sys_errno

        # basic http stuff
        long status_code
        object status_line
        object body
        object headers
        object headers_str

        # curl related info
        object effective_url
        double total_time
        double namelookup_time
        double connect_time
        double speed_download
        object primary_ip
        long primary_port

    def get_error(self):
        """
            Returns (<int>errnum, <string>Error rescription.)
            Calling this method only makes sense when HttpResponse.success == False
        """
        if self.curl_ret != CURLE_OK:
            return (self.curl_ret, easy_strerror(self.curl_ret))
        elif self.sys_errno:
            return (self.sys_errno, strerror(self.sys_errno))
        return (0, 'No error. All ok.')


cdef class CurlThread:

    """
        C curl thread abstraction.
        For each object of this class C pthread will be created.
        Generally application would only need 1 instance of this class.

        General usage:

        c_thread = psy_curl.CurlThread()

        # setup callback ResponseCallback for
        # file descriptor returned by c_thread.signal_fd()
        # for instance: using pyev.Io

        c_thread.fetch("http://example.com")

        # when request will be done signal_fd will become readable
        # and as a consequense ResponseCallback will get called.
        # In ResponseCallback a typical useage:

        def signal_fd_readable_cb():
            for rsp in c_thread.get_complete():
                if not rsp.success:
                    print rsp.get_error()
                else:
                    print rsp.status_code
                    print rsp.status_line
                    print rsp.headers
                    print rsp.body

        # good cleanup process
        c_thread.join()

        # if you stopping thread with running tasks - it is a good idea
        # to read responses after thread joined, most of responses though
        # will be fail.
        complete_responses = c_thread.get_complete()

        # stop watching for read events on signal_fd.
    """

    cdef:
        thread_work_info_t * cthread
        int max_in_progress
        int joined

    cdef readonly:
        int tasks_in_progress

    def __cinit__(self, int curls = 5, int in_queue_size = 20, int out_queue_size = 50):

        self.cthread = psycurl_start_thread(curls, curls, in_queue_size, out_queue_size)
        if self.cthread == NULL:
            PyErr_SetFromErrno(OSError)
            return

        self.tasks_in_progress = 0
        self.max_in_progress = in_queue_size
        self.joined = 0


    def is_running(self):
        if self.cthread.thread_running:
            return True
        return False

    def get_error(self):
        """
            Will help to understand what happened when CurlThread
            suddenly exit because of error in curl worker thread.
        """
        if self.cthread.curl_m_exit_code:
            return (self.cthread.curl_m_exit_code, curl_multi_strerror(self.cthread.curl_m_exit_code))
        elif self.cthread.curl_exit_code:
            return (self.cthread.curl_exit_code, curl_easy_strerror(self.cthread.curl_exit_code))
        elif self.cthread.sys_errno:
            return (self.cthread.sys_errno, strerror(self.cthread.sys_errno))
        return (0, 'No error. All ok.')


    def get_complete(self):
        """
            Method should be called when a signal_fd is readable.
            Returns list of HttpResponse objects.
            Calling this method clears read event on signal_fd.
        """
        global errno
        cdef:
            tcurl_task_t * task
            tcurl_task_t * task_copy
            int ret, i, header_offset
            HttpResponse rsp
            char * c_tmp
            mem_chunk_t * m_chunk

        result = []
        ret = psycurl_get_complete(self.cthread, &task)
        if ret != 0:
            PyErr_SetFromErrno(OSError)
            return

        task_copy = task
        try:
            while task:

                rsp = HttpResponse.__new__(HttpResponse)
                rsp.curl_ret = task.curl_ret
                rsp.sys_errno = task.sys_errno
                rsp.id = <object><PyObject *>task.id
                if task.curl_ret != CURLE_OK or task.success != 1:
                    rsp.success = 0
                    task = task.next
                    result.append(rsp)
                    continue

                rsp.success = 1
                rsp.status_code = task.response_code

                # body processing
                rsp.body = PyString_FromStringAndSize(NULL, task.rsp_size)
                c_tmp = PyString_AS_STRING(rsp.body)
                m_chunk = task.rsp_data_head
                while m_chunk:
                    memcpy(c_tmp, m_chunk.data, m_chunk.used)
                    c_tmp += m_chunk.used
                    m_chunk = m_chunk.next

                if task.headers_cnt > 0:
                    rsp.status_line = PyString_FromStringAndSize(NULL, task.headers_offset[0])
                    memcpy(PyString_AS_STRING(rsp.status_line), task.headers, task.headers_offset[0])

                if task.headers_cnt > 1:
                    # set headers string
                    i = task.headers_offset[task.headers_cnt - 1] - task.headers_offset[0]
                    rsp.headers_str = PyString_FromStringAndSize(NULL, i)
                    memcpy(PyString_AS_STRING(rsp.headers_str), task.headers + task.headers_offset[0], i)

                    if task.parse_headers:
                        headers = []
                        header_offset = task.headers_offset[0]
                        for i in range(1, task.headers_cnt - 1):
                            py_tmp = PyString_FromStringAndSize(
                                task.headers + header_offset,
                                task.headers_offset[i] - header_offset
                            )
                            header_offset = task.headers_offset[i]
                            headers.append(py_tmp)
                        rsp.headers = headers
                else:
                    rsp.headers = []
                    rsp.headers_str = ""
                rsp.effective_url = PyString_FromString(task.effective_url)
                rsp.total_time = task.total_time
                rsp.namelookup_time = task.namelookup_time
                rsp.connect_time = task.connect_time
                rsp.speed_download = task.speed_download
                rsp.primary_ip = PyString_FromString(task.primary_ip)
                rsp.primary_port = task.primary_port

                result.append(rsp)

                task = task.next
        finally:
            # free all no matter what happens
            task = task_copy
            while task:
                self.tasks_in_progress -= 1
                task_copy = task.next
                self.free_task(task)
                task = task_copy

        return result

    def signal_fd(self):
        """
            returns file descriptor (int)
            Read event on this descriptor indicates that
            there are complete responses available.
            CurlThread.get_complete() when whis happens.
        """
        return self.cthread.out_signal_fd[0]


    cdef free_task(self, tcurl_task_t * task):
        cdef:
            tcurl_option_t *opt_tmp, *opt_tmp_free

        # free options
        opt_tmp = task.options
        while opt_tmp:
            if opt_tmp.pyobjref:
                Py_XDECREF(<PyObject *>opt_tmp.pyobjref)
            if opt_tmp.p_type == SlistParam:
                curl_slist_free_all(opt_tmp.slist_param)
            opt_tmp_free = opt_tmp
            opt_tmp = opt_tmp.next
            PyMem_Free(opt_tmp_free)

        if task.id:
            # free 1 ref because (void *)task->id
            # no longer hold ref to this obj.
            Py_XDECREF(<PyObject *>task.id)

        # free body chunks
        mem_chunk_free(task.rsp_data_head)
        task.rsp_data_head = NULL
        task.rsp_data_cur = NULL

        free(task.effective_url)
        free(task.primary_ip)
        PyMem_Free(task)


    def fetch(self, url,
              headers = None,
              body = None,
              options = None,
              unsigned int unzip = 1,
              int parse_headers = 1):
        """
            Method that puts http response to internal C thread queue.
            @url - http url
            @body - body for POST request.
                    It should be properly encoded.
                    This libray and libcurl is NOT doing any encoding of POST body.
            @options - dictionary {int : string or int}
                        see: http://curl.haxx.se/libcurl/c/curl_easy_setopt.html 
                        for list of options.
            @unzip - If set to 1 and Content-encoding: gzip in response headers:
                     then HttpResponse.body will be ungziped.
                     Setting to 0 disables ungzip of http body.
        """
        global errno
        cdef:
            tcurl_task_t * task
            tcurl_option_t *opt_tmp, *opt_tmp2
            int ret
            int errno_save
            curl_slist *slist

        # sanity checks
        if self.joined:
            raise psy_exc_exception(EBADF, "Thread is not running.")

        if self.tasks_in_progress >= self.max_in_progress:
            raise psy_exc_queue_full(EAGAIN,
                "Queue is full. Consider increasing queue size or try again later.")


        # in parameters type checks
        if headers is not None and not PyList_Check(headers):
            raise TypeError("headers must be of type list got %s instead." % type(headers))

        if options is not None and not PyDict_Check(options):
            raise TypeError("options must be of type dict got %s instead." % type(options))

        if body is not None and not PyString_Check(body):
            raise TypeError("body must be string type got %s instead." % type(body))

        if not PyString_Check(url):
            raise TypeError("url must be string type got %s instead." % type(url))


        # allocate task
        task = <tcurl_task_t *>PyMem_Malloc(sizeof(tcurl_task_t))
        if task == NULL:
            raise MemoryError("Can't allocate task")
        memset(task, 0x00, sizeof(tcurl_task_t))

        task.rsp_data_head = NULL
        task.rsp_data_cur = NULL

        task.id = NULL

        task.should_inflate = max(unzip, 1)
        task.parse_headers = parse_headers

        try:
            # set url option
            task.options = <tcurl_option_t *>PyMem_Malloc(sizeof(tcurl_option_t))
            task.options.option = CURLOPT_URL
            task.options.c_param = PyString_AS_STRING(url)
            task.options.p_type = CharPtrParam
            task.options.pyobjref = <void *>url
            Py_XINCREF(<PyObject *>url)
            task.options.next = NULL

            task_id = object()
            task.id = <void *>task_id
            # this makes task.id.ob_refcnt == 2
            # 1 ref for python (this function return)
            # 1 ref: we holding in (void *) prt above
            Py_XINCREF(<PyObject *>task.id)

            # set other options
            opt_tmp = task.options
            if options:
                for k,v in options.iteritems():
                    # XXX add more rigorous option check
                    if not PyInt_Check(k):
                        raise TypeError("invalid curl option: %s" % repr(k))

                    opt_tmp2 = <tcurl_option_t *>PyMem_Malloc(sizeof(tcurl_option_t))
                    if opt_tmp2 == NULL:
                        raise MemoryError("Can't allocate option")
                    memset(opt_tmp2, 0x00, sizeof(tcurl_option_t))

                    # append to list
                    opt_tmp.next = opt_tmp2
                    opt_tmp = opt_tmp2

                    opt_tmp.option = <CURLoption>k
                    if PyInt_Check(v):
                        opt_tmp.p_type = LongParam
                        opt_tmp.l_param = v
                    elif PyString_Check(v):
                        opt_tmp.p_type = CharPtrParam
                        opt_tmp.c_param = PyString_AS_STRING(v)
                        opt_tmp.pyobjref = <void *>v
                        Py_XINCREF(<PyObject *>v)

                        if <CURLoption>k == CURLOPT_READDATA:
                            task.read_data = PyString_AS_STRING(v)
                            task.read_left = PyString_GET_SIZE(v)
                    else:
                        raise TypeError("option: %s value type: %s invalid." % (k, type(v)))

            # set headers
            if headers:
                slist = NULL
                for h in headers:
                    slist = curl_slist_append(slist, h)

                opt_tmp2 = <tcurl_option_t *>PyMem_Malloc(sizeof(tcurl_option_t))
                if opt_tmp2 == NULL:
                    curl_slist_free_all(slist)
                    raise MemoryError("Can't allocate option for headers.")
                opt_tmp2.option = CURLOPT_HTTPHEADER
                opt_tmp2.slist_param = slist
                opt_tmp2.p_type = SlistParam
                opt_tmp2.pyobjref = <void *>headers
                Py_XINCREF(<PyObject *>headers)
                opt_tmp2.next = NULL
                opt_tmp.next = opt_tmp2
        except:
            self.free_task(task)
            raise

        # put to queue
        ret = psycurl_put_task(self.cthread, task)
        if ret != 0:
            errno_save = errno
            self.free_task(task)
            errno = errno_save
            if errno == EAGAIN:
                raise psy_exc_queue_full(EAGAIN,
                            "Queue is full. Consider increasing queue size or try again later.")
            else:
                PyErr_SetFromErrno(OSError)
                return

        self.tasks_in_progress += 1
        return task_id


    def join(self):
        """
            Join underlying C thread in a bloking way.
            This method may take some time.
        """
        if self.cthread.thread_running:
            psycurl_stop_thread(self.cthread)
            self.joined = 1
        else:
            raise psy_exc_exception(EINVAL, "psy_curl_thread already joined.")

    def __dealloc__(self):
        cdef:
            tcurl_task_t * task
            tcurl_task_t * task_next

        # join thread if running
        if not self.joined:
            psycurl_stop_thread(self.cthread)

        # read & free complete tasks
        ret = psycurl_get_complete(self.cthread, &task);
        while task:
            task_next = task.next
            self.free_task(task)
            task = task_next

        # free thread
        psycurl_free_thread(self.cthread)
        self.cthread = NULL




