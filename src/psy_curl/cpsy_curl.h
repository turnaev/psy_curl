
#ifndef CEASY_CURL_H
#define CEASY_CURL_H


#ifndef _GNU_SOURCE
    #define _GNU_SOURCE
#endif

#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>

#include <curl/multi.h>

#include <zlib.h>

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

#define PSYC_MAX_HEADERS_SIZE 8192
#define PSYC_MAX_HEADERS 128

#define SIGNAL_BUF_SIZE 256
#define DEFAULT_POLL_TIMEOUT 100

#define GZIP_INFLATE_BUF 131072


typedef struct tcurl_easy_ll_t tcurl_easy_ll_t;
typedef struct tcurl_option_t  tcurl_option_t;
typedef struct tcurl_task_t tcurl_task_t;
typedef struct mem_chunk_t mem_chunk_t;

typedef enum param_type {
    LongParam,
    CharPtrParam,
    SlistParam,
} param_type;

struct mem_chunk_t {
    size_t size;
    size_t used;
    mem_chunk_t * next;
    char data[];
};


typedef struct tcurl_easy_ll_t {
    CURL * eh;
    int in_multi;
    tcurl_task_t * cur_task;
    tcurl_easy_ll_t * next;
} tcurl_easy_ll_t;


typedef struct tcurl_option_t {
    CURLoption option;
    param_type p_type;

    union {
        char * c_param;
        long l_param;
        struct curl_slist *slist_param;
    };

    void * pyobjref;
    tcurl_option_t * next;
} tcurl_option_t;



typedef struct tcurl_task_t {
    int success;
    void * id;

    tcurl_option_t * options;

    // data to be sent with request
    char * read_data;
    uint64_t read_left;

    CURLcode curl_ret;
    int sys_errno;

    // response headers
    char headers[PSYC_MAX_HEADERS_SIZE];
    uint16_t headers_offset[PSYC_MAX_HEADERS];
    uint16_t headers_cnt;

    // flag
    int parse_headers;

    /* flag 1 - intention to gunzip
     * 2 - means there is content-encoding: gzip header
     * and thus we should inflate
     */
    unsigned int should_inflate;

    // summary size of rsp_data_head body chunks
    size_t rsp_size;
    mem_chunk_t * rsp_data_head; // response data head
    mem_chunk_t * rsp_data_cur; // response data current


    long response_code; // http response code

    // see curl on this
    char * effective_url;
    double total_time;
    double namelookup_time;
    double connect_time;
    double speed_download;
    char * primary_ip;
    long primary_port;

    // pointer to next task
    tcurl_task_t * next;
} tcurl_task_t;

typedef struct thread_work_info_t {

    pthread_t thread;
    int thread_running;

    // response body allocated
    // by this size chunks
    long body_page_size;

    int in_signal_fd[2]; // signal fds about new tasks in queue
    int out_signal_fd[2]; // signals about complete tasks
    int exit_signal_fd[2];

    // number of curl_easy handles
    int curls_num;
    // number of max simul connections
    long maxconns;

    // task queue
    tcurl_task_t * in_task_queue;
    tcurl_task_t * in_task_queue_tail;
    int in_task_queue_len;
    int in_queue_max_len;

    // rsp queue
    tcurl_task_t * rsp_queue_head;
    tcurl_task_t * rsp_queue_tail;
    int rsp_queue_len;
    int out_queue_max_len;

    // locks
    pthread_mutex_t in_lock;
    pthread_mutex_t out_lock;

    // exit codes of thread
    CURLMcode curl_m_exit_code;
    CURLcode curl_exit_code;
    int sys_errno;
} thread_work_info_t;


thread_work_info_t * psycurl_start_thread(int curls_num,
        long maxconns,
        int in_queue_size,
        int out_queue_size
        );
void psycurl_stop_thread(thread_work_info_t * self);
void psycurl_free_thread(thread_work_info_t * self);

int psycurl_get_complete(thread_work_info_t * self, tcurl_task_t ** result);

/*
 * API function
 * should be used to enqueue tasks
 */
int psycurl_put_task(thread_work_info_t * self, tcurl_task_t * new_task);

// int psycurl_read_debug(thread_work_info_t * self, debug_chunk_t ** result);
void mem_chunk_free(mem_chunk_t * chunk);


#endif


