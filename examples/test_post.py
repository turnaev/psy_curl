#!/usr/bin/env python

import sys
sys.path.append("../build/lib.linux-x86_64-2.7")

import signal
import xmlrpclib

import pyev
import psy_curl

URL = "http://rpc.id.rambler.ru/rpc"


def make_xml_args(xml_args):
    result = {}
    for name, value in xml_args.iteritems():
        if name in result:
            raise Exception("double argument for %s (type %d)" % \
                (name, type))
        elif '.' in name:
            names = name.split('.')
            last = names.pop()
            this = result
            for n in names:
                if n not in this:
                    this[n] = {}
                this = this[n]
            this[last] = value
        else:
            result[name] = value
            #raise ValueError, "make_xml_args support only named arguments"
    return (result, )

def xmlrpc_body(method_name, xml_rpc_args):
    return "<?xml version='1.0'?>\n" + ''.join([
        '<methodCall>\n',
        '<methodName>', method_name, '</methodName>\n',
        xmlrpclib.dumps( make_xml_args(xml_rpc_args) ),
        '</methodCall>'])

def response_ready_cb(w, events):
    c_thread = w.data
    read_responses(c_thread)

def read_responses(c_thread):
    global rsp_cnt, fetch_success_cnt, fetch_error_cnt
    complete = c_thread.get_complete()
    for rsp in complete:
        if not rsp.success:
            print "rsp error: %s" % repr(rsp.get_error())
        else:
            print "status_code: %d" % rsp.status_code
            print repr(rsp.status_line)
            print repr(rsp.headers_str)
            print rsp.headers
            print "body len: %d" % len(rsp.body)
            print repr(rsp.body)

def sigint_cb(w, events):
    c_thread = w.data
    c_thread.join()
    read_responses(c_thread)
    w.loop.stop(pyev.EVBREAK_ALL)


def produce_task_cb(w, events):
    c_thread = w.data

    body = xmlrpc_body('Rambler::Id::check_rsid', {'rsid' : '8d5449ab3cafcf56f8fab6de5fc6ec94'})
    options = {
        psy_curl.POST : 1,
        psy_curl.USERAGENT : 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.72 Safari/537.36',
        psy_curl.READDATA : body,
        psy_curl.TIMEOUT_MS: 1500,
    }
    headers_d = {
        "Accept-Encoding" : "gzip",
        "Connection" : "Keep-Alive",
        "Cookie" : "ruid=HQAAAJZ7QFFshfUNATXXAAB=; ",
        'Content-Type' : 'text/xml',
    }
    headers = ["%s: %s" % (k,v) for k,v in headers_d.iteritems()]
    c_thread.fetch(URL, headers = headers, options = options, body = body, parse_headers = 1)




loop = pyev.default_loop( debug = True )

c_thread = psy_curl.CurlThread()

# produce get requests cb
t = loop.timer(0.5, 1.0, produce_task_cb, c_thread)
t.start()

# response ready cb
w = loop.io(c_thread.signal_fd(), pyev.EV_READ, response_ready_cb, c_thread)
w.start()

# sigint cb
stop_w = loop.signal(signal.SIGINT, sigint_cb, c_thread)
stop_w.start()

loop.start()


