#!/usr/bin/env python

import sys
sys.path.append("../build/lib.linux-x86_64-2.7")
import signal

import pyev
import psy_curl

URL = "http://mtnp.rambler.ru/r_static/index.html"

def response_ready_cb(w, events):
    c_thread = w.data
    read_responses(c_thread)

def read_responses(c_thread):
    complete = c_thread.get_complete()
    for rsp in complete:
        if not rsp.success:
            print "rsp error: %s" % repr(rsp.get_error())
        else:
            print "status_code: %d" % rsp.status_code
            print repr(rsp.status_line)
            print repr(rsp.headers_str)
            print rsp.headers
            print "body len: %d" % len(rsp.body)
            print repr(rsp.body)

def sigint_cb(w, events):
    c_thread = w.data
    c_thread.join()
    read_responses(c_thread)
    w.loop.stop(pyev.EVBREAK_ALL)


def produce_task_cb(w, events):
    c_thread = w.data

    options = {
        psy_curl.TIMEOUT_MS : 6200,
        psy_curl.CONNECTTIMEOUT_MS : 5000,
    }
    headers_d = {
        "Accept-Encoding" : "gzip",
        "Connection" : "Keep-Alive",
        "User-Agent" : "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.72 Safari/537.36",
    }
    headers = ["%s: %s" % (k,v) for k,v in headers_d.iteritems()]
    try:
        c_thread.fetch(URL, headers = headers, options = options)
    except psy_curl.psy_exc_queue_full:
        print "can't put request, fetch queue is full."


loop = pyev.default_loop( debug = True )

c_thread = psy_curl.CurlThread()

# produce get requests cb
t = loop.timer(0.5, 1.0, produce_task_cb, c_thread)
t.start()

# response ready cb
w = loop.io(c_thread.signal_fd(), pyev.EV_READ, response_ready_cb, c_thread)
w.start()

# sigint cb
stop_w = loop.signal(signal.SIGINT, sigint_cb, c_thread)
stop_w.start()

loop.start()


