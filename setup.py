# -*- coding: utf8 -*-

# ultraviolance distutils hack
import re
import distutils.versionpredicate
distutils.versionpredicate.re_validPackage = re.compile(r"^\s*([a-z_-]+)(.*)", re.IGNORECASE)

import os
from distutils.core import setup
from distutils.extension import Extension

try:
    import xpkg
except ImportError:
    pass

SRC_PATH = 'src/psy_curl'

def is_package(path):
    return (
        os.path.isdir(path) and
        os.path.isfile(os.path.join(path, '__init__.py'))
        )

def find_packages(path, base="", root_packages = None):
    """ Find all packages in path """
    packages = {}
    for item in os.listdir(path):
        dir = os.path.join(path, item)
        if is_package( dir ):
            if base:
                module_name = "%s.%s" % (base, item)
            else:
                module_name = item
            if root_packages:
                for pkg in root_packages:
                    if module_name[:len(pkg)]  == pkg:
                        break
                else:
                    # not a root package
                    continue

            packages[module_name] = dir
            packages.update(find_packages(dir, module_name))
    return packages


def src_path(*args):
    return [os.path.join(SRC_PATH,x) for x in args ]


extra = {}
try:
    from Cython.Distutils import build_ext
    extra = dict(cmdclass={'build_ext': build_ext})
    psy_curl  = Extension('psy_curl', src_path('psy_curl.pyx') + src_path('cpsy_curl.c'),
                          #extra_compile_args=['-O0', "-std=gnu99", "-g"],
                          extra_compile_args=['-O3', "-std=gnu99"],
                          libraries = ['curl', 'z'],
                          #extra_link_args=["-g"]
                         )
except ImportError:
    psy_curl  = Extension('psy_curl', src_path('psy_curl.c') + src_path('cpsy_curl.c'),
                          #extra_compile_args=['-O0', "-std=gnu99", "-g"],
                          extra_compile_args=['-O3', "-std=gnu99"],
                          libraries = ['curl', 'z'],
                          #extra_link_args=["-g"]
                         )



#packages = find_packages("./src", root_packages = ['xsyslog'])

setup (
        name='psy_curl',
        version='0.1.5',
        packages= {},
        #packages=packages,
        #package_dir = {'xsyslog' : SRC_PATH},
        author='Evgeny Turnaev',
        author_email='turnaev.e@gmail.com',
        description='libcurl python bindings',
        long_description="""Syslog improved""",

        classifiers = ['devel'],
        options={'build_pkg': {'name_prefix': True,
                               'python_min_version': 2.7,
                              }},
        ext_modules=[psy_curl],
        #provides = packages.keys(),
        **extra
        )



